import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class AlphaBankTestClass2 {
    public static void main(String[] args) {
        //Считываем строку из файла
        String s = input("./src/AlphaBankTestTask_2.txt");
        // Парсим строку
        int[] numbers = parseString(s);
        //Сортируем массив по возрастанию
        numbers = ascendingSort(numbers);
        //Выводим результат в консоль
        printArray(numbers);
        System.out.println();
        //Сортируем массив по убыванию
        numbers = descendingSort(numbers);
        //Выводим результат в консоль
        printArray(numbers);
    }

    //private static String split();
    private static String input(String filePath) {
        String s = null;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            s = bufferedReader.readLine();
        } catch (FileNotFoundException e) {
            System.out.println("Ошибка! Файл, находящийся по адресу, " + filePath + " не найден!");
        } catch (IOException e) {
            System.out.println("Ошибка вывода данных из файла!");
        }
        return s;
    }

    private static int[] parseString(String s) {
        String[] sequance = s.split(",");
        int[] numbers = new int[sequance.length];
        for (int i = 0; i < sequance.length; i++) {
            numbers[i] = Integer.parseInt(sequance[i]);
        }
        return numbers;
    }

    private static int[] ascendingSort(int[] numbers) {
        Arrays.sort(numbers);
        return numbers;
    }

    private static int[] descendingSort(int[] numbers) {
        int buf;
        for (int i = 0; i < numbers.length; i++) {
            for (int j = 0; j < numbers.length - 1; j++) {
                if (numbers[j] < numbers[j + 1]) {
                    buf = numbers[j + 1];
                    numbers[j + 1] = numbers[j];
                    numbers[j] = buf;
                }
            }
        }
        return numbers;
    }

    private static void printArray(int[] numbers) {
        for (int i : numbers) {
            System.out.print(i + " ");
        }
    }
}
